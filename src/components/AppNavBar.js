import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {useContext} from 'react';
import {Fragment} from 'react';
import UserContext from '../UserContext.js';
import {Link, NavLink} from 'react-router-dom';

export default function AppNavBar (){

	// primary color: #15ab24
	const {user} = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg" id="navbar">
		      <Container>
			        <Navbar.Brand as = {Link} to = "/">Asia amb Gracia</Navbar.Brand>
			        <Navbar.Toggle aria-controls="basic-navbar-nav" />
			        <Navbar.Collapse id="basic-navbar-nav">
				          <Nav className="me-auto">
				            <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
				            {/*Conditional Rendering*/}
				            {
				            	user ?
				            	<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
				            	:
				            	<Fragment>
				            		<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
				            		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
				            	</Fragment>
				            }
				            <NavDropdown title="Menu" id="basic-nav-dropdown">
				              <NavDropdown.Item as = {NavLink} to = "/menu">Full Menu</NavDropdown.Item>
				              <NavDropdown.Item as = {NavLink} to = "/menu/wok">Wok</NavDropdown.Item>
				              <NavDropdown.Item as = {NavLink} to = "/menu/toppings">Toppings</NavDropdown.Item>
				              <NavDropdown.Item as = {NavLink} to = "/menu/sauce">Sauce</NavDropdown.Item>
				              <NavDropdown.Divider />
				              <NavDropdown.Item href="https://www.asiaambgracia.com/web/">
				                About Us
				              </NavDropdown.Item>
				            </NavDropdown>
				          </Nav>
			        </Navbar.Collapse>
		      </Container>
	    </Navbar>
		)
}