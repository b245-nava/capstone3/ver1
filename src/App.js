// React items
import './App.css';
import {useState, useEffect} from 'react';

// Router
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

// UserProvider
import {UserProvider} from './UserContext.js';

// components
import AppNavBar from './components/AppNavBar.js';

// pages
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';

function App() {
  
  // UserContext set-up
  const [user, setUser] = useState(null);

  useEffect(() => {
    console.log(user)
  }, [user]);

  const unSetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_CAPSTONE2}/user/myProfile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {

      if(localStorage.getItem('token') !== null){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser(null);
      }
    })
  }, []);

  return (
      <UserProvider value = {{user, setUser, unSetUser}}>
          <Router>
            <AppNavBar/>
            <Routes>
                <Route path = "/" element = {<Home/>}/>
                <Route path = "/register" element = {<Register/>}/>
                <Route path = "/login" element = {<Login/>}/>
                <Route path = "/logout" element = {<Logout/>}/>
                <Route path = "*" element = {<NotFound/>}/>
            </Routes>
          </Router>
      </UserProvider>
  );
}

export default App;
