import {Link} from 'react-router-dom';
import {Container} from 'react-bootstrap';

export default function NotFound (){

	return(
		<Container className = "text-center mt-5">
			<h1>404</h1>
			<img className = "my-2" src = 'https://thumbs.gfycat.com/BronzeDizzyGrasshopper-size_restricted.gif' alt="Gif of the 'These are not the droids you are looking for' scene from 'Star Wars: A New Hope' by Lucasfilms."/>
			<h3 className = "mt-2">There's nothing here.</h3>
			<h4>(This is not the page you are looking for.)</h4>
			<h5 className = "text-muted">Head back <Link to = "/">home</Link></h5>
		</Container>
		)
}