import {Container, Button, Form} from 'react-bootstrap';
import {useState, useContext, useEffect} from 'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function Login (){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const navigate = useNavigate();

	// Code that will allow us to consume the UserContext object and its properties for user validation
	const {user, setUser} = useContext(UserContext);

	// console.log(user);

	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);


	const login = (event) => {
		event.preventDefault();

		// Process a fetch request to correspond with our back-end API
		fetch(`${process.env.REACT_APP_CAPSTONE2}/user/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);

			if(data === false){
				Swal.fire({
					title: 'Incorrect email or password.',
					icon: 'error',
					text: 'Please try again or register.'
				});
			} else {
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(localStorage.getItem('token'));

				Swal.fire({
					title: "Login successful!",
					icon: 'success'
				});
				
				navigate("/");
			}
		})
		.catch(err => console.log(err))
	};

	const retrieveUserDetails = (token) => {

		// The token sent as part of the request's header information

		fetch(`${process.env.REACT_APP_CAPSTONE2}/user/myProfile`, {
			headers: {
				Authorization: `Bearer ${token}`
			}	
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	};

	return(

		user ?
		<Navigate to ="*"/>
		:

		<Container>
			<h1 className = "text-center mt-5">Log In</h1>
			<Form className = "mt-5">
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email Address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter email" 
			        	value = {email}
			        	onChange = {event => setEmail(event.target.value)}
			        	required
			        	/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password"
			        	value = {password}
			        	onChange = {event => setPassword(event.target.value)}
			        	required
			        	/>
			        <Form.Text className="text-muted">
			        	No account yet? Don't miss our latest news and offers! Register <Link to = "/register">here.</Link>
			        </Form.Text>
			      </Form.Group>

			      {
			      	isActive ?
			      	<Button variant="success" onClick = {event => login(event)}>
			      	  Log In
			      	</Button>
			      	:
			      	<Button variant="danger" type="submit" disabled>
			      	  Log In
			      	</Button>
			      }
		    </Form>
		    </Container>
		)
};