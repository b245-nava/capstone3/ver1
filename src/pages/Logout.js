import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext.js';

export default function Logout () {

	const{setUser, unSetUser} = useContext(UserContext);

	useEffect(() => {
		unSetUser();
		setUser(null);
	}, []);

	return(
		<Navigate to = "/login"/>
		)
};