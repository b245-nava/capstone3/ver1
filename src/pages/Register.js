import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Fragment, useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2';

export default function Register () {

	// UseStates
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPass, setConfirmPass] = useState("");
	const [isActive, setIsActive] = useState(false);

	// UserContext + Navi
	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() => {
		if(email !== "" && password !== "" && confirmPass !== "" && password === confirmPass){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password, confirmPass])

	// register()
	function register (event) {
		event.preventDefault();
		
		fetch(`${process.env.REACT_APP_CAPSTONE2}/user/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
			if(data){
				Swal.fire({
					title: 'You have been registered. Thank you very much!',
					icon: 'success'
				});
				navigate('/login');
			} else {
				Swal.fire({
					title: 'Registration error!',
					text: 'Or did you mean to log in?',
					icon: 'error'
				});
			}
		})
	};

	return(
		user ?
		<Navigate to = "*"/>
		:
		<Fragment>
		<Container>
			<h1 className = "text-center mt-5">Register</h1>
			<Form className = "mt-5" onSubmit = {event => register(event)}>
				<Form.Group className="mb-3" controlId="formBasicEmail">
				  <Form.Label>Email address</Form.Label>
				  <Form.Control 
				  	type="email" 
				  	placeholder="Enter email" 
				  	value = {email}
				  	onChange = {event => setEmail(event.target.value)}
				  	required
				  	/>
				  <Form.Text className="text-muted">
				    We'll never share your personal data with anyone else.
				  </Form.Text>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formBasicPassword">
				  <Form.Label>Password</Form.Label>
				  <Form.Control 
				  	type="password" 
				  	placeholder="Password"
				  	value = {password}
				  	onChange = {event => setPassword(event.target.value)}
				  	required
				  	/>
				</Form.Group>

				<Form.Group className="mb-1" controlId="formConfirmPassword">
				  <Form.Label>Confirm Password</Form.Label>
				  <Form.Control 
				  	type="password" 
				  	placeholder="Confirm your password"
				  	value = {confirmPass}
				  	onChange = {event => setConfirmPass(event.target.value)}
				  	required
				  	/>
				</Form.Group>

				<Form.Group className="mb-3">
					<Form.Text className="text-muted">
						Already have an account? Log in <Link to = "/login">here.</Link>
					</Form.Text>
				</Form.Group>

				{/*In this code block, we have an example of ternary operation and conditional rendering, which depends on the state of the variant isActive.*/}
				{
					isActive ?
					<Button variant="success" type="submit">
					  Submit
					</Button>
					:
					<Button variant="danger" type="submit" disabled>
					  Submit
					</Button>
				}
			</Form>
		</Container>
		</Fragment>
		)
}