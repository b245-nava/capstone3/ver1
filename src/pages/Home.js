import {Container, Row, Col} from 'react-bootstrap';
import {Fragment} from 'react';

export default function Home () {

	return(
		<Fragment>
			<Container id = "landing-img">
				<Container>
					{/*text, if wanted, here*/}
				</Container>
			</Container>
			<Container>
				{/*basic info and content here*/}
				{/*socials here*/}
			</Container>
		</Fragment>
		)
}